import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert
} from "react-native";
import { API_BASE_URL } from "../Utils/Constants";
import axios from "axios";
import { Actions } from "react-native-router-flux";

export default class FormRegister extends Component {
  constructor() {
    super();
    this.register = this.register.bind(this);
    url = API_BASE_URL + "/auth/register";
    this.state = {
      email: "",
      noHp: "",
      pass: "",
      name: "",
      username: "",
      loading: false
    };
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
  }
  register(event) {
    
    this.setState({
      loading: true
    });
    let data = JSON.stringify({
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      username: this.state.name,
      noHp: this.state.hp,
      groupId: "00"
    });
    console.log("request : " + data);
    if (
      this._isMounted &&
      this.state.name &&
      this.state.password &&
      this.state.name &&
      this.state.hp
    ) {
      event.preventDefault();
      console.log("request : " + data);
      axios
        .post(url, data, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(res => {
          const response = JSON.stringify(res.data);
          const { status, message } = JSON.parse(response);
          console.log(status + " " + message);
          if (status === "success") {
            console.log("response : " + response);
            // Actions.popTo("login");
            Actions.reset("login");
            Actions.refresh();
          } else {
            setTimeout(() => {
              Alert.alert("Warning", "tidak mendapatkan response");
            }, 100);
          }
        })
        .catch(error => {
          console.log("error : " + error);
          setTimeout(() => {
            Alert.alert("Kesalahan", "Terjadi Kesalahan pada server!");
          }, 100);
        });
    } else {
      setTimeout(() => {
        Alert.alert("Warning", "Terdapat field yang kosong!");
      }, 100);
    }
  }

  componentWillUnmount() {
    // source.cancel("tutup fetch");
    this._isMounted = false;
  }
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.inputBox}
          underlineColorAndroid="#AEA"
          placeholder="Name Lengkap"
          placeholderTextColor="#FFF"
          selectionColor="#FFF"
          keyboardType="twitter"
          name="name"
          onChangeText={text => this.setState({ name: text })}
        />

        <TextInput
          style={styles.inputBox}
          underlineColorAndroid="#AEA"
          placeholder="Email"
          placeholderTextColor="#FFF"
          selectionColor="#FFF"
          keyboardType="email-address"
          name="email"
          onChangeText={text => this.setState({ email: text })}
        />

        <TextInput
          style={styles.inputBox}
          underlineColorAndroid="#AEA"
          placeholder="Nomor Hp"
          placeholderTextColor="#FFF"
          selectionColor="#FFF"
          keyboardType="numeric"
          name="hp"
          onChangeText={text => this.setState({ hp: text })}
        />

        <TextInput
          style={styles.inputBox}
          underlineColorAndroid="#AEA"
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor="#FFF"
          name="password"
          onChangeText={text => this.setState({ password: text })}
          ref={input => (this.password = input)}
        />

        <TextInput
          style={styles.inputBox}
          underlineColorAndroid="#AEA"
          placeholder="Re-Password"
          secureTextEntry={true}
          name="repass"
          onChangeText={text => this.setState({ repass: text })}
          placeholderTextColor="#FFF"
          ref={input => (this.password = input)}
        />

        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText} onPress={this.register}>
            {this.props.type}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  inputBox: {
    width: 300,
    backgroundColor: "#AEA",
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: "#ffffff",
    marginVertical: 10
  },

  button: {
    width: 300,
    backgroundColor: "#1bac04",
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },

  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center"
  }
});
