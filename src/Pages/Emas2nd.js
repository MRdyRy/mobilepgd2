import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image
} from "react-native";
import { Actions } from "react-native-router-flux";
import { LineChart } from "react-native-chart-kit";
import { API_BASE_URL } from "../Utils/Constants";
import axios from "axios";

const BannerWidth = Dimensions.get("window").width;
const datas = {
  labels: ["21/7", "22/7", "23/7", "24/7", "25/7", "26/7"],
  datasets: [
    {
      data: [20, 45, 28, 80, 99, 43],
      // color: (opacity = 1) => "rgba(134, 65, 244, ${opacity})", // optional
      strokeWidth: 2 // optional
    }
  ]
};
const chartConfig = {
  backgroundGradientFrom: "#FFF",
  backgroundGradientTo: "#FFF",
  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
  strokeWidth: 2 // optional, default 3
};

export default class Emas2nd extends React.Component {
  constructor(props) {
    super(props);
    url = API_BASE_URL + "/hargaemas";
    this.state = {
      hargaBeli: "",
      hargaJual: "",
      tglBerlaku: ""
    };
    this._isMounted = true;
  }

  componentDidMount() {
    console.log(url);
    this._isMounted = true;
    if (this._isMounted) {
      axios
        .get(url)
        .then(res => {
          const { hargaBeli, hargaJual, tglBerlaku } = JSON.parse(
            res.data.data
          );
          this.setState({
            hargaBeli: hargaBeli,
            hargaJual: hargaJual,
            tglBerlaku: tglBerlaku
          });
        })
        .catch(error => {
          if (axios.isCancel(error)) {
            console.log("rollback");
          } else {
            console.log(error);
          }
        });
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  home() {
    Actions.pop();
  }
  render() {
    return (
      <View>
        <View style={styles.shadow}>
          <View style={styles.innerheader}>
            <TouchableOpacity onPress={this.home}>
              <Image
                style={{ width: 40, height: 40 }}
                source={{
                  uri:
                    "https://img.icons8.com/flat_round/64/000000/circled-left.png"
                }}
              />
            </TouchableOpacity>
            <Text style={styles.bannertext}>Tabungan Emas</Text>
          </View>
          {/* <View style={styles.innerheader}>
              <Text style={styles.bannertext}>Tabungan Emas</Text>
            </View> */}
        </View>

        {/* tes grafik */}
        <View style={{ marginTop: 5, backgroundColor: "#FFF" }}>
          <Text style={{ marginLeft: 3 }}>Harga Emas</Text>
          <LineChart
            data={datas}
            width={BannerWidth}
            height={220}
            chartConfig={chartConfig}
          />
          <View>
            <Text>Harga Jual : Rp.{this.state.hargaJual},-</Text>
            <Text>Harga Beli : Rp.{this.state.hargaBeli},-</Text>
            <Text>Tanggal Berlaku : {this.state.tglBerlaku}</Text>
          </View>
        </View>
        <View style={styles.container}>
          <TouchableOpacity onPress={this.tabunganEmas}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri: "https://img.icons8.com/dusk/64/000000/gold-bars.png"
                }}
              />
              <Text style={styles.info}>Pendaftaran </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.inquiryHarga}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri: "https://img.icons8.com/dusk/64/000000/expensive-2.png"
                }}
              />
              <Text style={styles.info}>Buyback</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.inquiryHarga}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri: "https://img.icons8.com/dusk/64/000000/expensive-2.png"
                }}
              />
              <Text style={styles.info}>Transfer</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
    marginTop: 5
  },
  banner: {
    padding: 5,
    marginLeft: 10,
    fontSize: 15
  },
  shadow: {
    backgroundColor: "#1bac04",
    marginBottom: 5,
    width: BannerWidth,
    height: 55,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5
  },
  innerheader: {
    padding: 3,
    alignItems: "flex-end",
    flexDirection: "row"
  },
  bannertext: {
    padding: 10,
    fontSize: 20,
    color: "#FFF"
  },
  menuBox: {
    backgroundColor: "#FFF",
    width: 110,
    height: 110,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3,
    margin: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5
  },
  icon: {
    width: 60,
    height: 60
  }
});
