import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert
} from "react-native";

import AsyncStorage from "@react-native-community/async-storage";
import { Actions } from "react-native-router-flux";
import axios from "axios";
import { API_BASE_URL } from "../Utils/Constants";

export default class Logo extends Component {
  constructor() {
    super();
    url = API_BASE_URL + "/auth/login";
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      usernameOrEmail: "",
      password: "",
      success: "",
      loading: false
    };
  }
  handleSubmit(event) {
    this.setState({
      loading: true
    });
    let data = JSON.stringify({
      usernameOrEmail: this.state.usernameOrEmail,
      password: this.state.password
    });
    if (this.state.usernameOrEmail && this.state.password) {
      event.preventDefault();
      console.log("request : " + data);
      axios
        .post(url, data, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(res => {
          const response = JSON.stringify(res.data.data);
          const { auth, accessToken, username, groupId } = JSON.parse(response);
          console.log("response : " + response);
          if (accessToken) {
            AsyncStorage.setItem("accessToken", JSON.stringify(accessToken));
            AsyncStorage.setItem("username", JSON.stringify(username));
            AsyncStorage.setItem("groupId", JSON.stringify(groupId));
            console.log("isi async : " + JSON.stringify(AsyncStorage._getKeys));
            Actions.home();
          } else {
            this.setState({ spinner: false });
            setTimeout(() => {
              Alert.alert("Warning", "Username / Password Salah!");
            }, 100);
          }
        })
        .catch(error => {
          if (error.status === 401) {
            setTimeout(() => {
              Alert.alert("Maaf", "Anda Belum Terdaftar!");
            }, 100);
            console.log("error : 401-UnAuthorized");
          } else {
            setTimeout(() => {
              Alert.alert("Kesalahan", "Terjadi Kesalahan pada server!");
            }, 100);
            console.log("error : " + error);
          }
        });
    } else {
      setTimeout(() => {
        Alert.alert("Warning", "Username / Password kosong!");
      }, 100);
    }
  }

  home() {
    Actions.home();
  }
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.inputBox}
          underlineColorAndroid="#AEA"
          placeholder="Email"
          placeholderTextColor="#FFF"
          selectionColor="#FFF"
          keyboardType="email-address"
          name="usernameOrEmail"
          onChangeText={text => this.setState({ usernameOrEmail: text })}
          // onSubmitEditing={() => this.password.focus()}
        />

        <TextInput
          style={styles.inputBox}
          underlineColorAndroid="#AEA"
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor="#FFF"
          name="password"
          secureTextEntry={true}
          onChangeText={text => this.setState({ password: text })}
        />

        <TouchableOpacity style={styles.button} onPress={this.handleSubmit}>
          <Text style={styles.buttonText}>{this.props.type}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  inputBox: {
    width: 300,
    backgroundColor: "#AEA",
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: "#ffffff",
    marginVertical: 10
  },

  button: {
    width: 300,
    backgroundColor: "#1bac04",
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },

  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center"
  }
});
