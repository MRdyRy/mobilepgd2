import React, { Component } from "react";
import { Router, Stack, Scene } from "react-native-router-flux";
import Login from "./Pages/Login";
import Signup from "./Pages/Signup";
import Home from "./Pages/Home";
import Profile from "./Pages/Profile";
import About from "./Pages/About";
import Emas1st from "./Pages/Emas1st";
import Emas2nd from "./Pages/Emas2nd";
import Amanah from "./Pages/Amanah";
import Mikro from  "./Pages/Mikro";
import MPO from "./Pages/MPO";
import History from "./Pages/History";
import BadaiEmas from "./Pages/BadaiEmas";
import Agen from './Pages/Agen';
import Gadai from './Pages/Gadai';
import Footer from "./Pages/footer";

export default class Routes extends Component {
  render() {
    return (
      <Router>
        <Stack key="root" hideNavBar={true}>
          <Scene key="login" component={Login} title="Login" initial={true} />
          <Scene key="signup" component={Signup} title="Register" />
          <Scene key="home" component={Home} title="Beranda" />
          <Scene key="profile" component={Profile} title="Profile" />
          <Scene key="amanah" component={Amanah} title="Produk Amanah" />
          <Scene key="mikro" component={Mikro} title="Produk Mikro" />
          <Scene key="mpo" component={MPO} title="Multy Payment Online" />
          <Scene key="history" component={History} title="Riwayat Transaksi" />
          <Scene key="badaiEmas" component={BadaiEmas} title="Promo Badai Emas" />
          <Scene key="outlet" component={Footer} title="Informasi Outlet" />
          <Scene key="about" component={About} title="About" />
          <Scene
            key="inquiry"
            component={Emas1st}
            title="Inquiry Harga Beli Emas"
          />
          <Scene key="tabungan" component={Emas2nd} title="Tabungan Emas" />
          <Scene key="agen" component={Agen} title="Agen Pegadaian" />
          <Scene key="gadai" component={Gadai} title="Produk Gadai" />
        </Stack>
      </Router>
    );
  }
}
