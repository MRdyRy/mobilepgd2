import React from "react";
import Carousel from "react-native-banner-carousel";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Alert,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import AsyncStorage from "@react-native-community/async-storage";

const BannerWidth = Dimensions.get("window").width;
const BannerHeight = 160;
const images = [
  "https://www.pegadaian.co.id/uploads/produk/a1f600653bd5794b952c4ad15c11d713_thumb.jpg",
  "https://www.pegadaian.co.id/uploads/produk/e2b811d1e7d157b81721cfd9e40b3aa4_thumb.jpg",
  "https://www.pegadaian.co.id/uploads/produk/c6b5c07490e6d7aeb6c0b9a54d761aa0_thumb.jpg"
];

export default class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      username: ""
    };
    AsyncStorage.getItem("username", (error, result) => {
      if (result) {
        let resultParsed = JSON.parse(result);
        console.log("parse : " + resultParsed);
        this.setState({
          username: resultParsed
        });
      }
    });
  }

  inquiryHarga() {
    Actions.inquiry();
  }
  tabunganEmas() {
    Actions.tabungan();
  }
  amanah() {
    Actions.amanah();
  }
  mikro() {
    Actions.mikro();
  }
  mpo() {
    Actions.mpo();
  }
  riwayat() {
    Actions.history();
  }
  badaiEmas() {
    Actions.badaiEmas();
  }
  outlet() {
    Actions.outlet();
  }
  Gadai() {
    Actions.gadai();
  }
  Agen() {
    Actions.agen();
  }
  about() {
    Actions.about();
  }
  riwayat() {
    Actions.history();
  }

  renderPage(image, index) {
    return (
      <View key={index}>
        <Image
          style={{ width: BannerWidth, height: BannerHeight }}
          source={{ uri: image }}
        />
      </View>
    );
  }
  render() {
    return (
      <View>
        <View style={styles.shadow}>
          <TouchableOpacity onPress={this.outlet}>
            <View style={styles.innerheader}>
              <Image
                style={{ width: 40, height: 40 }}
                source={{
                  uri: "https://img.icons8.com/dusk/64/000000/person-male.png"
                }}
              />
              <Text style={styles.username}>{this.state.username}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <Carousel
          autoplay
          autoplayTimeout={5000}
          loop
          index={0}
          pageSize={BannerWidth}
          containerCustomStyle={{ flex: 1 }}
          slideStyle={{ flex: 1 }}
        >
          {images.map((image, index) => this.renderPage(image, index))}
        </Carousel>
        <View style={styles.container}>
          <TouchableOpacity onPress={this.tabunganEmas}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri: "https://img.icons8.com/dusk/64/000000/gold-bars.png"
                }}
              />
              <Text style={styles.info}>Tabungan Emas</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.inquiryHarga}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri: "https://img.icons8.com/dusk/64/000000/expensive-2.png"
                }}
              />
              <Text style={styles.info}>Mulia</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.amanah}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri: "https://img.icons8.com/dusk/64/000000/car.png"
                }}
              />
              <Text style={styles.info}>Amanah</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.mikro}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri: "https://img.icons8.com/dusk/64/000000/budget.png"
                }}
              />
              <Text style={styles.info}>Mikro</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.mpo}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri: "https://img.icons8.com/dusk/64/000000/online-store.png"
                }}
              />
              <Text style={styles.info}>MPO</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.Gadai}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri:
                    "https://img.icons8.com/dusk/48/000000/payment-history.png"
                }}
              />
              <Text style={styles.info}>Gadai</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.Agen}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri:
                    "https://img.icons8.com/dusk/48/000000/payment-history.png"
                }}
              />
              <Text style={styles.info}>Agen</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.riwayat}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri:
                    "https://img.icons8.com/dusk/48/000000/payment-history.png"
                }}
              />
              <Text style={styles.info}>Riwayat</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.about}>
            <View style={styles.menuBox}>
              <Image
                style={styles.icon}
                source={{
                  uri:
                    "https://img.icons8.com/dusk/48/000000/payment-history.png"
                }}
              />
              <Text style={styles.info}>About</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 5 }}>
          <TouchableOpacity onPress={this.badaiEmas}>
            <Image
              style={{ width: BannerWidth, height: 150 }}
              source={{
                uri:
                  "https://www.pegadaian.co.id/uploads/slides/3a83d6d926529fbced9e3d8f8dc06eca_thumb.jpg"
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   // backgroundColor: "#fff",
  //   alignItems: "center",
  //   justifyContent: "center"
  // }

  container: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center"
  },
  menuBox: {
    marginTop: 10,
    backgroundColor: "#FFF",
    width: 110,
    height: 110,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3,
    margin: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5
  },
  icon: {
    width: 60,
    height: 60
  },
  info: {
    fontSize: 13,
    color: "#1bac04"
  },
  shadow: {
    backgroundColor: "#fff",
    marginBottom: 5,
    width: BannerWidth,
    height: 55,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5
  },
  innerheader: {
    padding: 3,
    alignItems: "flex-end",
    flexDirection: "row"
  },
  username: {
    fontSize: 20,
    color: "#085c04"
  },
  // saldo: {
  //   flexDirection: "row-reverse",
  //   position: "absolute",
  //   alignContent: "flex-end",
  //   alignSelf: "flex-end",
  //   marginTop: -30
  // }
});
